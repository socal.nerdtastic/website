---
title: pdfgrep 2.1.1 released
published: 2018-05-22
author: Hans-Peter Deifel
---
This is a very small bugfix release that fixes the build with
`libunac` support enabled, i.e `./configure --with-unac`. Otherwise
it's the same as 2.1.0. As usual, the tarball is available at the
[download page](/download.html).
