---
title: pdfgrep - Download
header: Download
---
<div class="content">

Look for pdfgrep in your OS' package manager, it's likely to be there!
Here are some platforms that include pdfgrep:

 - [Debian] (and its derivates like [Ubuntu])
 - [Arch Linux]
 - [Fedora]
 - Red Hat Enterprise Linux and CentOS (via [Fedora] EPEL)
 - [openSUSE]
 - [Gentoo Linux]
 - Mac OS X (via [MacPorts] or [Homebrew])
 - [OpenBSD]
 - [FreeBSD]
 
If your distribution doesn't have it, you'll have to download the
source code and compile it on your own. The code is available as a
.tar.gz file:

<p class="columns">
<div class="column is-offset-1">
<a href="$latesttar$" class="button is-primary"><span class="icon"><i class="fa fa-download"/></span><span>Download Source Code (v$latestversion$)</span></a>
</div>
</p>

You can verify the integrity of the download with the following
information:

 - [GPG signature]($signature$)
 - SHA256 sum: `$sha256$`

## Git

The current development version can be obtained with:

```
git clone https://gitlab.com/pdfgrep/pdfgrep.git
```

But be aware that this may be more unstable than the released version.

## Install

To compile and install the source code, use the standard linux triad:

```
./configure
make
sudo make install
```

See the `README` file or `configure --help` for more info or read the
(very extensive) `INSTALL` file in the source.

If you're using the git version, you will also have to run
`./autogen.sh` in advance.

## Older versions

Older versions are also available for download:

<div class="scrollable">

Version | SHA256 | Tarball | Signature |
--------|--------|---------|-----------
$for(downloads)$$version$|`$sha256$`|[.tar.gz]($url$)|$if(sig)$[GPG Signature]($sig$)$else$-$endif$
$endfor$

</div>


[Debian]: https://packages.debian.org/pdfgrep
[Ubuntu]: https://packages.ubuntu.com/pdfgrep
[Fedora]: https://apps.fedoraproject.org/packages/pdfgrep
[Gentoo Linux]: https://packages.gentoo.org/package/app-text/pdfgrep
[Arch Linux]: https://www.archlinux.org/packages/community/x86_64/pdfgrep/
[openSUSE]: https://software.opensuse.org/package/pdfgrep
[OpenBSD]: https://cvsweb.openbsd.org/cgi-bin/cvsweb/ports/textproc/pdfgrep/
[FreeBSD]: http://portsmon.freebsd.org/portoverview.py?category=textproc&portname=pdfgrep
[Homebrew]: http://formulae.brew.sh/formula/pdfgrep
[MacPorts]: https://www.macports.org/ports.php?by=name&substr=pdfgrep
</div>
