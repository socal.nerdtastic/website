{-# LANGUAGE OverloadedStrings, ScopedTypeVariables, LambdaCase #-}

import Hakyll
import Data.Monoid
import Data.List.Split
import Data.Char
import Data.List
import Data.Function
import Control.Monad
import Control.Arrow
import Control.Applicative
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Char8 as SB
import Crypto.Hash (hashlazy, digestToHexByteString, SHA256, Digest)
import Data.Maybe
import System.FilePath (takeDirectory)

conf :: Configuration
conf = defaultConfiguration {
  deployCommand = "./scripts/deploy.sh",
  providerDirectory = "./site/"
}

main :: IO ()
main = hakyllWith conf $ do
  match "images/*" $ do
    route idRoute
    compile copyFileCompiler

  match "js/*" $ do
    route idRoute
    compile copyFileCompiler -- TODO minimize js

  match "css/style.scss" $ do
    route (setExtension "css")
    compile compressScssCompiler

  match "css/font-awesome/css/font-awesome.min.css" $ do
    route idRoute
    compile copyFileCompiler

  match "css/font-awesome/fonts/*" $ do
    route idRoute
    compile copyFileCompiler

  match "templates/*" $
    compile templateCompiler

  match "index.html" $ do
    route idRoute
    compile $ do
      ctx <- getDownloadContext

      getResourceBody
        >>= applyAsTemplate ctx
        >>= loadAndApplyTemplate "templates/default.html" ctx
        >>= relativizeUrls

  match "contact.md" $ do
    route $ setExtension "html"
    compile $ do
      ctx <- getContext

      getResourceBody
        >>= applyAsTemplate ctx
        >>= renderPandoc
        >>= loadAndApplyTemplate "templates/page.html" ctx
        >>= loadAndApplyTemplate "templates/default.html" ctx
        >>= relativizeUrls

  match "manpages/*" $ do
    route $ setExtension "html"
    compile $ do
      res <- manpageCompiler >>= saveSnapshot "content"

      manpages <-  (map fst . highestFirst) <$> loadAllSnapshots "manpages/*" "content"

      let ctx = listField "manpages" manCtx (return manpages) <> manCtx

      loadAndApplyTemplate "templates/doc.html" ctx res
        >>= loadAndApplyTemplate "templates/default.html" manCtx
        >>= relativizeUrls


  create ["doc.html"] $ do
    route idRoute
    compile $ do
      manpages <-  (map fst . highestFirst) <$> loadAllSnapshots "manpages/*" "content"
      let (manpage, vers) = (id &&& extractVersion) $ head manpages

      let ctx = mconcat
                [ constField "version" $ showVersion vers
                , constField "title"   $ "pdfgrep - Manpage for " ++ showVersion vers
                , listField "manpages" manCtx (return manpages)
                , defaultContext
                ]

      makeItem (itemBody manpage :: String)
        >>= loadAndApplyTemplate "templates/doc.html" ctx
        >>= loadAndApplyTemplate "templates/default.html" ctx
        >>= relativizeUrls

  create ["manpages.html"] $ do
    route idRoute
    compile $ do
      manpages <- (map fst . highestFirst) <$> loadAll "manpages/*"

      let ctx =  constField "title" "pdfgrep - Manpages"
              <> listField "manpages" manCtx (return manpages)
              <> defaultContext

      makeItem ""
        >>= loadAndApplyTemplate "templates/manpages.html" ctx
        >>= loadAndApplyTemplate "templates/default.html" ctx
        >>= relativizeUrls

  match "download/*.tar.gz" $ do
    route idRoute
    compile getResourceLBS

  match "download/*.asc" $ do
    route idRoute
    compile copyFileCompiler

  match "download.md" $ do
    route $ setExtension "html"
    compile $ do
      ctx <- getDownloadContext

      (downloads :: [Item ByteString]) <- (map fst . highestFirst) <$> loadAll "download/*.tar.gz"

      let ctx' = ctx <> listField "downloads" tarCtx (return downloads)

      getResourceBody
        >>= applyAsTemplate ctx'
        >>= renderPandoc
        >>= loadAndApplyTemplate "templates/page.html" ctx'
        >>= loadAndApplyTemplate "templates/default.html" ctx'
        >>= relativizeUrls


  match "news/*" $ do
    route $ setExtension "html"
    compile $ do
      ctx <- getContext

      let ctx' = constField "header" "News" <> ctx

      pandocCompiler
        >>= saveSnapshot "content"
        >>= loadAndApplyTemplate "templates/post.html" ctx'
        >>= loadAndApplyTemplate "templates/page.html" ctx'
        >>= loadAndApplyTemplate "templates/default.html" ctx'
        >>= relativizeUrls

  match "news.html" $ do
    route idRoute

    compile $ do
      ctx <- getContext

      news <- recentFirst =<< loadAllSnapshots "news/*" "content"

      let ctx' = ctx <> listField "news" ctx (return news)

      getResourceBody
        >>= applyAsTemplate ctx'
        >>= loadAndApplyTemplate "templates/page.html" ctx'
        >>= loadAndApplyTemplate "templates/default.html" ctx'
        >>= relativizeUrls

getContext :: Compiler (Context String)
getContext = do
  (item :: Item ByteString, vers) <- highestVersion <$> loadAll "download/*.tar.gz"
  return $ mconcat
    [ constField "latestversion" (showVersion vers)
    , constField "latesttar" (toFilePath $ itemIdentifier item)
    , dateField "date" "%B, %e %Y"
    , defaultContext
    ]

getDownloadContext :: Compiler (Context String)
getDownloadContext = do
  (item :: Item ByteString, vers) <- highestVersion <$> loadAll "download/*.tar.gz"
  return $ mconcat
    [ constField "latestversion" (showVersion vers)
    , constField "latesttar" (toFilePath $ itemIdentifier item)
    , field "signature" $ \_ -> fromMaybe "" <$> getSignature item
    , field "sha256" $ \_ -> hashSum item
    , dateField "date" "%B, %e %Y"
    , defaultContext
    ]

manpageCompiler :: Compiler (Item String)
manpageCompiler = getResourceBody >>= withItemBody callAsciidoc
  where callAsciidoc = unixFilter "asciidoc" args
        args = [ "-b", "html5"
               , "-s"
               , "-a", "disable-javascript"
               , "-"
               ]

-- | Create a SCSS compiler that transpiles the SCSS to CSS and
-- minifies it (relying on the external 'sass' tool)
--
-- taken from https://codetalk.io/posts/2016-05-10-compiling-scss-and-js-in-hakyll.html
compressScssCompiler :: Compiler (Item String)
compressScssCompiler = do
  fmap (fmap compressCss) $ do
    dir <- takeDirectory <$> getResourceFilePath
    getResourceString
      >>= withItemBody (unixFilter "sass" [ "-s"
                                          , "--scss"
                                          , "--style", "compressed"
                                          , "--load-path", dir
                                          ])

manCtx :: Context String
manCtx = mconcat
  [ field "version" $ return . showVersion . extractVersion
  , field "title"   $ \item ->
      return ("pdfgrep - Manpage for " ++ showVersion (extractVersion item))
  , defaultContext
  ]


tarCtx :: Context ByteString
tarCtx = mconcat
  [ urlField "url"
  , field "version" $ return . showVersion . extractVersion
  , field "sig" $ getSignature >=> \case
      Just name -> pure name
      Nothing -> empty
  , field "sha256" hashSum
  ]

getSignature :: Item a -> Compiler (Maybe String)
getSignature item = getRoute sigIdent

  where name = toFilePath $ itemIdentifier item
        sigFile = name ++ ".asc"
        sigIdent = fromFilePath sigFile

hashSum :: Item ByteString -> Compiler String
hashSum = return . SB.unpack . digestToHexByteString . sha256 . itemBody
  where sha256 :: ByteString -> Digest SHA256
        sha256 = hashlazy

-- pdfgrep-1.3.1 => [1,3,1]
extractVersion :: Item a -> [Int]
extractVersion = extract . toFilePath . itemIdentifier
  where extract = map read . filter (liftM2 (&&) (not.null) (all isDigit)) . splitOneOf "-."

highestVersion :: [Item a] -> (Item a, [Int])
highestVersion = head . highestFirst

highestFirst :: [Item a] -> [(Item a, [Int])]
highestFirst = sortBy (flip compare `on` snd) . map (id &&& extractVersion)

showVersion :: [Int] -> String
showVersion = intercalate "." . map show
