#!/bin/bash

rsync -av --delete _site/ ../pdfgrep_deploy/ --exclude .git || exit 1
cd ../pdfgrep_deploy/
git add --all || exit 1
git commit -m "Deploy" || exit 1
git status || exit 1

echo ""
read -p "Really push? [y/n]: " -n 1 -r
echo ""

if [[ $REPLY =~ ^[Yy]$ ]]
then
    git push || exit 1
fi
